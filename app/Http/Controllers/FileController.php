<?php

namespace App\Http\Controllers;

use App\Common\Errors\CustomError;
use App\Jobs\ProcessUploadedFile;
use App\Repositories\Contracts\FileRequestRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FileController extends Controller
{

    protected FileRequestRepositoryInterface $fileRequestRepository;

    public function __construct(FileRequestRepositoryInterface $fileRequestRepository)
    {
        $this->fileRequestRepository = $fileRequestRepository;
    }

    /**
     * Upload file from client
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function uploadFile(Request $request): JsonResponse
    {
        $file = $request->file('file');
        $newFileName = Str::random(40) . '.' . $file->getClientOriginalExtension();
        if (!$file) {
            return $this->sendClientErrorResponse(CustomError::NO_FILE_ERROR);
        }
        $path = Storage::disk('public')->putFileAs('images', $file, $newFileName);
        $fileRequest = $this->fileRequestRepository->create(['path' => $path]);
        $fileRequestJob = (new ProcessUploadedFile($fileRequest))->onQueue('new_requests');
        $this->dispatch($fileRequestJob);
        return $this->sendSuccessResponse(['id' => $fileRequest->id], 200);
    }
}
