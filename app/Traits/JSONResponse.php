<?php

namespace App\Traits;

use App\Common\Errors\CustomError;
use Exception;
use Throwable;

trait JSONResponse
{
    /**
     * Send successful JSON response to the client
     *
     * @param $data
     * @param int $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendSuccessResponse($data, int $statusCode = 200)
    {
        $response = [
            'data' => $data
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Send unsuccessful client based responses
     *
     * @param array $errors
     * @param int   $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendClientErrorResponse(array $errors, int $statusCode = 422)
    {
        $response = [
            'errors' => $errors
        ];
        return response()->json($response, $statusCode);
    }

    /**
     * Send server errors
     *
     * @param Throwable $ex
     * @param int       $statusCode
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendServerErrorResponse(Throwable $ex, int $statusCode = 500)
    {
        $response = [
            'errors' => [
                CustomError::FATAL_ERROR
            ]
        ];
        return response()->json($response, $statusCode);
    }
}
