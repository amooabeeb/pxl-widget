<?php

namespace App\Traits;

use Carbon\Carbon;

trait DataFilter
{
    /**
     * Determine if document should be processed
     *
     * @param array $data
     *
     * @return bool
     */
    public function canProcessDocument(array $data): bool
    {
        $formattedDOB = str_replace('/', '-', $data['date_of_birth']);
        $now = Carbon::now();
        $age = $now->diffInYears($formattedDOB);
        if ($age < 18 || $age > 65) {
            return false;
        }
        return true;
    }

    /**
     * Get file extension from path. Should probably go to an Util package
     *
     * @param string $path
     *
     * @return string
     */
    public function getExtensionFromPath(string $path): string
    {
        return preg_match('/\./', $path) ? preg_replace('/^.*\./', '', $path) : '';
    }
}
