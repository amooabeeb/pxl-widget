<?php

namespace App\Providers;

use App\Repositories\Concrete\FileRequestRepository;
use App\Repositories\Concrete\UserRepository;
use App\Repositories\Contracts\FileRequestRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(FileRequestRepositoryInterface::class, FileRequestRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
