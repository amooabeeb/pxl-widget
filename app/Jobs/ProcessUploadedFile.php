<?php

namespace App\Jobs;

use App\Adapters\FileReaderInterface;
use App\Models\FileRequest;
use App\Repositories\Contracts\FileRequestRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Traits\DataFilter;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ProcessUploadedFile implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, DataFilter;

    public int $tries = 2;

    public int $timeout = 600;

    protected FileRequest $fileRequest;

    /**
     * Create a new job instance.
     *
     * @param FileRequest $fileRequest
     */
    public function __construct(FileRequest $fileRequest)
    {
        $this->fileRequest = $fileRequest;
    }

    /**
     * Execute the job.
     *
     * @param UserRepositoryInterface $userRepository
     * @param FileRequestRepositoryInterface $fileRequestRepository
     * @param FileReaderInterface $fileReader
     *
     * @return void
     *
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public function handle(
        UserRepositoryInterface $userRepository,
        FileRequestRepositoryInterface $fileRequestRepository,
        FileReaderInterface $fileReader
    ) {

        $processedNumber = ($fileRequestRepository->getById($this->fileRequest->id))->number_processed;

        $filePath = Storage::disk('public')->url($this->fileRequest->path);
        $extension = $this->getExtensionFromPath($this->fileRequest->path);

        // Get decoded json per object as stream
        $decodedContentStream = $fileReader->parseContents($filePath, $extension);

        foreach ($decodedContentStream as $key => $document) {
            // Don't process already processed documents
            if ($processedNumber > $key) {
                continue;
            }
            if ($this->canProcessDocument($document)) {
                $userRepository->create($document);
                $fileRequestRepository->update($this->fileRequest->id, ['number_processed' => $key]);
            }
        }
    }
}
