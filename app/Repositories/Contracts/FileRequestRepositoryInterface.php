<?php

namespace App\Repositories\Contracts;

use App\Models\FileRequest;
use Illuminate\Database\Eloquent\Collection;

interface FileRequestRepositoryInterface
{
    /**
     * Create a file request
     *
     * @param array $data
     *
     * @return FileRequest
     */
    public function create(array $data): FileRequest;

    /**
     * Get file request by id
     *
     * @param int $id
     *
     * @return FileRequest
     */
    public function getById(int $id): FileRequest;

    /**
     * Get file request by id
     *
     * @param int $id
     * @param array $data
     *
     * @return FileRequest
     */
    public function update(int $id, array $data): FileRequest;

}
