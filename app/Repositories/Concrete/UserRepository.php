<?php

namespace App\Repositories\Concrete;

use App\Models\CreditCard;
use App\Models\User;
use App\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;
use DateTime;

class UserRepository implements UserRepositoryInterface
{

    protected User $user;

    protected CreditCard $creditCard;

    public function __construct(User $user, CreditCard $creditCard)
    {
        $this->user = $user;
        $this->creditCard = $creditCard;
    }

    private function createCreditCard(array $data): CreditCard
    {
        $creditCard = new CreditCard();
        $creditCard->type = $data['type'];
        $creditCard->number = $data['number'];
        $creditCard->name = $data['name'];
        $creditCard->expiration_date = Carbon::createFromFormat('m/y', ($data['expirationDate']))->subMonthNoOverflow();
        return $creditCard;
    }

    public function create(array $data): User
    {
        $creditCard = $this->createCreditCard($data['credit_card']);
        $formattedDOB = str_replace('/', '-', $data['date_of_birth']);
        $user = new User();
        $user->name = $data['name'];
        $user->address = $data['address'];
        $user->checked = $data['checked'];
        $user->description = $data['description'];
        $user->interest = $data['interest'];
        $user->email = $data['email'];
        $user->account = $data['account'];
        $user->date_of_birth = Carbon::parse($formattedDOB);
        $user->save();
        $user->creditCard()->save($creditCard);
        return $user;
    }
}
