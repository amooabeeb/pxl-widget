<?php

namespace App\Repositories\Concrete;

use App\Models\FileRequest;
use App\Repositories\Contracts\FileRequestRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class FileRequestRepository implements FileRequestRepositoryInterface
{

    protected FileRequest $fileRequest;

    public function __construct(FileRequest $fileRequest)
    {
        $this->fileRequest = $fileRequest;
    }

    /**
     * Create file request
     *
     * @param array $data
     *
     * @return FileRequest
     */
    public function create(array $data): FileRequest
    {
        return $this->fileRequest->create($data);
    }

    /**
     * Get file request by id
     *
     * @param int $id
     *
     * @return FileRequest
     */
    public function getById(int $id): FileRequest
    {
        return $this->fileRequest->where('id', $id)->first();
    }

    /**
     * @param int $id
     * @param array $data
     *
     * @return FileRequest
     */
    public function update(int $id, array $data): FileRequest
    {
        $this->fileRequest->where('id', $id)->update($data);
        return $this->getById($id);
    }

}
