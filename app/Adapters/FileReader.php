<?php

namespace App\Adapters;

class FileReader implements FileReaderInterface
{

    protected $fileReader;

    /**
     *
     * @param string $filePath
     * @param string $extension
     *
     */
    public function parseContents(string $filePath, string $extension)
    {
        switch ($extension) {
            case 'json':
                return (new JsonFileReaderAdapter())->parseContentsSync($filePath);
            default:
                return null;
        }
    }
}
