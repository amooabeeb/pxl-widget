<?php

namespace App\Adapters;

use JsonMachine\JsonMachine;

class JsonFileReaderAdapter implements FileReaderAdapterInterface
{
    /**
     * Method to pass content of file
     *
     * @param string $filePath
     *
     */
    public function parseContentsSync(string $filePath)
    {
        //:Todo  Modify JsonMachine to return a decoded array of the current object
        return JsonMachine::fromFile($filePath, '');
    }
}
