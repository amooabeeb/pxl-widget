<?php

namespace App\Adapters;

use JsonMachine\JsonMachine;

interface FileReaderAdapterInterface
{
    /**
     * Parse Contents of a file
     *
     * @param string $filePath
     *
     */
    public function parseContentsSync(string $filePath);
}
