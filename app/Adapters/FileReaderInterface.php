<?php

namespace App\Adapters;

interface FileReaderInterface
{

    public function parseContents(string $filePath, string $extension);
}
