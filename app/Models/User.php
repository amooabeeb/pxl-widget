<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class User extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'address', 'checked', 'description', 'interest', 'account', 'email', 'date_of_birth'];

    /**
     * Relationship with Credit Card
     *
     * @return HasOne
     */
    public function creditCard(): HasOne
    {
        return $this->hasOne(CreditCard::class);
    }
}
