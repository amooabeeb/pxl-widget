<?php

namespace App\Common\Errors;

class CustomError
{
    public const FATAL_ERROR = [
        'title' => 'Fatal error',
        'detail' => 'Something went wrong'
    ];

    public const NO_FILE_ERROR = [
        'title' => 'File is required',
        'detail' => 'Please upload a valid json file'
    ];
}
