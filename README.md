# A file processing laravel background Service

---

## Table of Contents

- [Development](#development)
- [FAQ](#faq)

---

## Development

### Prerequisites

> Ensure you have docker installed on your local machine. You can install docker from https://docs.docker.com/get-docker/

### Setup

> Create a .env file by running `cp .env.example .env` and fill in the variables like

```
DB_HOST=database
DB_DATABASE={database name here}
DB_USERNAME={database user here}
DB_PASSWORD={database password here}
```

> Run `docker-compose up` to start the container or `docker-compose up -d` to start the container in
> background mode

### Package Installation

Check into the container and install relevant packages using the command below

```
docker exec -it <container_name> bash | composer install
```

### Application Setup

To migrate databases, run
```
docker exec -it <container_name> bash | php artisan migrate
```

To make uploaded files publicly accessible for processing, run

```
docker exec -it <container_name> bash | php artisan storage:link
```


### Accessing the app

> You can now access each of the services on port 8050


## FAQ

---
---

## Issues
### Common Local setup issues

> Backend services cannot connect to database

> Please make sure that the value of `DB_HOST` in your `.env` for your services is set to `database`
