FROM ubuntu:20.04 AS build

# Ensure we're using UTC
ENV TZ=Etc/UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

# Install services
RUN apt-get update && apt-get install -y software-properties-common unzip curl nginx supervisor

# Install PHP
RUN LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php && apt update && apt-get install -y \
    php7.4-fpm \
    php7.4-common \
    php7.4-curl \
    php7.4-mysql \
    php7.4-mbstring \
    php7.4-json \
    php7.4-xml \
    php7.4-bcmath \
    php7.4-zip \
    php7.4-cli

# Update php.ini values
RUN sed -E -i -e 's/max_execution_time = 30/max_execution_time = 120/' /etc/php/7.4/fpm/php.ini \
 && sed -E -i -e 's/memory_limit = 128M/memory_limit = 512M/' /etc/php/7.4/fpm/php.ini \
 && sed -E -i -e 's/post_max_size = 8M/post_max_size = 2048M/' /etc/php/7.4/fpm/php.ini \
 && sed -E -i -e 's/upload_max_filesize = 2M/upload_max_filesize = 2048M/' 	/etc/php/7.4/fpm/php.ini

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

EXPOSE 80
ENTRYPOINT ["/usr/bin/supervisord"]

# -----------------------------
# Setup our service application
# -----------------------------

WORKDIR /var/www/service
COPY --chown=1000:www-data . .

# Fetch the PHP dependencies
RUN composer install

# PHP configurations
RUN mv www.conf /etc/php/7.4/fpm/pool.d/www.conf
RUN mkdir -p /var/run/php

# Setup nginx
RUN mv app.nginx /etc/nginx/sites-enabled/default

# Setup supervisor
RUN mv supervisord.conf /etc/supervisor/conf.d/supervisord.conf
RUN mkdir -p /var/log/supervisor
